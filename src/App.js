import React, { lazy, Suspense } from "react";
import { connect, useSelector } from "react-redux";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "./assets/scss/style.scss";
import Loader from "./components/loader";
const Login = lazy(() => import("./pages/login"));
const Dashboard = lazy(() => import("./pages/dashboard"));

function App() {
  const loaderState = useSelector((state) => state.isLoader.isLoading);
  console.log(loaderState);
  return (
    <div className="App">
      {loaderState && <Loader />}
      <Router>
        <Suspense fallback={<Loader />}>
          <Switch>
            <Route exact path="/" component={Login} />
            <Route exact path="/dashboard" component={Dashboard} />
          </Switch>
        </Suspense>
      </Router>
    </div>
  );
}

export default connect()(App);
