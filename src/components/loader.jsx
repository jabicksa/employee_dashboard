import React from "react";

export default function Loader() {
  return (
    <div className="loader-wrapper">
      <img
        src="https://miro.medium.com/max/1158/1*9EBHIOzhE1XfMYoKz1JcsQ.gif"
        alt=""
      />
    </div>
  );
}
