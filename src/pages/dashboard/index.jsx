import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";

export default function Dashboard() {
  const [empData, setEmpData] = useState([]);
  const employeesDatas = useSelector(
    (state) => state.employeesDatas.employeesData
  );
  useEffect(() => {
    setEmpData(employeesDatas);
  }, []);
  return (
    <div className="dashboard-page">
      <header className="header">
        <div className="container">
          <h1>Employee List</h1>
        </div>
      </header>
      <section className="dashboard-section">
        <div className="container">
          <div className="table-wrapper">

          <table>
            <thead>
              <tr>
                <th>id</th>
                <th>name</th>
                <th>age</th>
                <th>gender</th>
                <th>email</th>
                <th>phoneNo</th>
              </tr>
            </thead>
            <tbody>
              {empData.map((employee, index) => {
                let { id, name, age, gender, email, phoneNo } = employee;
                return (
                  <tr key={index}>
                    <td>{id}</td>
                    <td>{name}</td>
                    <td>{age}</td>
                    <td>{gender}</td>
                    <td>{email}</td>
                    <td>{phoneNo}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
          </div>
        </div>
      </section>
    </div>
  );
}
