import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { loader } from "../../redux/actions/loaderAction";
import { login } from "../../redux/actions/loginActions";
import LoginValidation from "./loginValidation";
import userJson from "../../json/login.json";

function Login(props) {
  const [fields, setfields] = useState({
    email: "",
    password: "",
  });
  const [error, setError] = useState("");
  const dispatch = useDispatch();
  const loginResponse = useSelector((state) => state.isAuth);

  const handleChange = (e) => {
    let { name, value } = e.target;
    setfields({
      ...fields,
      [name]: value,
    });
  };

  const onFormSubmit = (e) => {
    e.preventDefault();
    const handleValidation = LoginValidation(fields);
    setError(handleValidation);
    if (Object.keys(handleValidation).length === 0) {
      dispatch(loader(true));
      if (
        fields.email === userJson.username &&
        fields.password === userJson.password
      ) {
        dispatch(login(true, fields));
        props.history.push("/dashboard");
      } else {
        dispatch(login(false, fields));
        alert("invalid User");
      }
      dispatch(loader(false));
    }
  };

  let { email, password } = fields;
  return (
    <div className="login-page">
      <div className="login-form-wrapper">
        <div className="form-container">
          <article>
            <p className="section-text">
              Welcome to Our <span> Dashboard</span>
            </p>
            <span className="section-sub-text">
              The Dashboard provides the Employee Details
            </span>
          </article>
          <form onSubmit={(e) => onFormSubmit(e)}>
            <div className="form-group">
              <label htmlFor="email">Email</label>
              <input
                type="text"
                name="email"
                id="email"
                placeholder="Enter Email Id"
                className="form-control"
                value={email}
                onChange={(e) => handleChange(e)}
              />
              {error.email && <p className="error-msg">{error.email}</p>}
            </div>
            <div className="form-group">
              <label htmlFor="password">Password</label>
              <input
                type="password"
                name="password"
                id="password"
                placeholder="******"
                className="form-control"
                value={password}
                onChange={(e) => handleChange(e)}
              />
              {error.password && <p className="error-msg">{error.password}</p>}
            </div>

            <button className="action-btn" type="submit">
              Login
            </button>
          </form>
        </div>
      </div>
    </div>
  );
}
export default Login;
