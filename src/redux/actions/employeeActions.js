import { ActionTypes } from "../constants/action-types";

export const employeeData = (data) => {
  return {
    type: ActionTypes.EMPLOYEES_LIST,
    payload: data,
  };
};
