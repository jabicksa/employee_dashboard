import { ActionTypes } from "../constants/action-types";

export const loader = (bool) => {
  let payload = {
    isLoading: bool,
  };
  return {
    type: ActionTypes.LOADER,
    payload,
  };
};
