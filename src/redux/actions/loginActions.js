import { ActionTypes } from "../constants/action-types";

export const login = (bool, user) => {
  let payload = {
    isAuth: bool,
    credential: user,
  };
  return {
    type: ActionTypes.LOGIN,
    payload,
  };
};
