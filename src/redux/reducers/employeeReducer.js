import { ActionTypes } from "../constants/action-types";
import empJson from "../../json/data.json";

const initialState = {
  employeesData: empJson,
};
const employeeReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ActionTypes.EMPLOYEES_LIST:
      return state;

    default:
      return state;
  }
};
export default employeeReducer;
