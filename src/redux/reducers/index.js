import { combineReducers } from "redux";
import employeeReducer from "./employeeReducer";
import loaderReducer from "./loaderReducer";
import loginReducer from "./loginReducer";

export const rootReducer = combineReducers({
  employeesDatas: employeeReducer,
  isAuth: loginReducer,
  isLoader: loaderReducer,
});
