import { ActionTypes } from "../constants/action-types";

const initialState = {
  isLoading: false,
};

const loaderReducer = (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.LOADER:
      return action.payload;
    default:
      return state;
  }
};

export default loaderReducer;
