import { ActionTypes } from "../constants/action-types";

const initialState = {
  isAuth: false,
  credential: {},
};
const loginReducer = (state = initialState, { type, payload }) => {
  console.log(payload);
  switch (type) {
    case ActionTypes.LOGIN:
      return payload;

    default:
      return state;
  }
};
export default loginReducer;
